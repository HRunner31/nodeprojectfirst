'use strict'

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

// cargar archivos de rutas
var project_routes = require('./routes/project');

// middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// CORS
// configurar cabeceras http
app.use((request, response, next) => {
    response.header('Access-Control-Allow-Origin', '*');
    response.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    response.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    response.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');

    next();
});

// RUTAS
app.use('/api', project_routes);

// app.get('/', (request, response) => {
//     response.status(200).send(
//         "<h1>Pagina de Inicio</h1>"
//     );
// });

// app.get('/test', (request, response) => {
//     response.status(200).send({
//         message: "Hola mundo desde mi API de NodeJS"
//     });
// });

// exportar
module.exports = app;