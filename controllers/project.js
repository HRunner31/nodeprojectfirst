'use strict'

var projectModel = require('../models/project');
var fs = require('fs');

var controller = {

    home: function (request, response) {
        return response.status(200).send({
            message: 'Soy la home'
        });
    },

    test: function (request, response) {
        return response.status(200).send({
            message: "Soy el metodo o acción test del controlador de project"
        });
    },

    saveProject: function (request, response) {
        var project = new projectModel();

        var params = request.body;
        project.name = params.name;
        project.description = params.description;
        project.category = params.category;
        project.year = params.year;
        project.langs = params.langs;
        project.image = null;

        project.save((error, projectStored) => {
            if (error) return response.status(500).send({ message: "Error al guardar el documento." });

            if (!projectStored) return response.status(404).send({ message: "No se ha podido guardar el proyecto." });

            return response.status(200).send({ project: "projectStored" });
        })

        return response.status(200).send({
            project: project,
            message: "Metodo saveProject"
        });
    },

    getProject: function (request, response) {
        var projectId = request.params.id;

        if (projectId == null) return response.status(404).send({ message: "El proyecto no existe." });

        projectModel.findById(projectId, (error, project) => {

            if (error) return response.status(500).send({ message: "Error al devolcer los datos." });

            if (!project) return response.status(404).send({ message: "El proyecto no existe" });

            return response.status(200).send({
                project
            });

        });
    },

    getProjects: function (request, response) {
        projectModel.find({}).sort('year').exec((error, projects) => {
            if (error) return response.status(500).send({ message: "Error al devolcer los datos." });

            if (!projects) return response.status(404).send({ message: "No hay proyectos para mostrar." });

            return response.status(200).send({
                projects
            });
        });
    },

    updateProject: function (request, response) {
        var projectId = request.params.id;

        var update = request.body;

        projectModel.findByIdAndUpdate(projectId, update, { new: true }, (error, projectUpdated) => {
            if (error) return response.status(500).send({ message: "Error al actualizar." });

            if (!projectUpdated) return response.status(404).send({ message: "No se ha podido actualizar el proyecto." });

            return response.status(200).send({
                project: projectUpdated
            });
        });
    },

    deleteProject: function (request, response) {
        var projectId = request.params.id;

        projectModel.findByIdAndRemove(projectId, (error, projectRemoved) => {
            if (error) return response.status(500).send({ message: "No se ha podido borrar el proyecto." });

            if (!projectRemoved) return response.status(404).send({ message: "No se ha podido eliminar el proyecto." });

            return response.status(200).send({
                project: projectRemoved
            });
        })
    },

    uploadImage: function (request, response) {
        var projectId = request.params.id;

        var fileName = 'Imagen no cargado.';

        if (request.files) {
            var filePath = request.files.image.path;
            var fileSplit = filePath.slit('\\');
            var fileName = fileSplit[1];
            var extSplit = fileName.split('\.');
            var fileExt = extSplit[1];

            if (fileExt == 'png' || fileExt == 'jpg' || fileExt == 'jpeg' || fileExt == 'gif') {
                projectModel.findByIdAndUpdate(projectId, { image: fileName }, { new: true }, (error, projectUpdated) => {
                    if (error) return response.status(500).send({ message: 'La imagen no ha cargado.' });

                    if (!projectUpdated) return response.status(404).send({ message: "El proyecto no existe y no se ha asignado la imagen." });
                });

                return response.status(200).send({
                    project: fileName
                });
            }

            else {
                fs.unlink(filePath, (error) => {
                    return response.status(200).send({
                        message: 'La extensión no es valida.'
                    });
                });
            }


        }
        else {
            return response.status(200).send({
                files: fileName
            });
        }

    }
};

module.exports = controller;